

- **TP1**:  Introduction au réseau local

    - Matériel didactique :ordinateur,imprimant,retour,cables, tableau

    - Ressources didactique : Support de Cours
    
    - Activités enseignant: Proposer aux apprenants cette sitauation : 
        Votre professeur désire récupérer les travaux que vous avez réalisés dans les ordinateurs de la classe, en vue de les évaluer sur son propre ordinateur.
        Premièrement l’existe d’un problème de connexion internet dans notre classe. L’absences des périphériques de stockage comme la clé USB.
        

    
    - Activités apprenants: Répondre a la Question suivante : 
        Qu’est-ce que vous proposez comme solution ?


- **TP2**:  Utilisation d'un réseau local

    - Matériel didactique :ordinateur,imprimant,retour,cables, tableau

    - Ressources didactique : Support de Cours

    - Activités enseignant: Proposer aux apprenants cette sitauation : 
        La salle d’informatique n’est équipée que d’une seule imprimante reliée à un seul ordinateur, celui du professeur.

    - Activités apprenants: Répondre a la Question suivante : 
        Comment pouvez-vous imprimer vos fichiers en utilisant cette imprimante ?